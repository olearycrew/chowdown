---

layout: recipe
title:  "Chow Mein"
image: mongolian-beef.jpg
imagecredit: https://flic.kr/p/8ANQTc
tags: mains, meat

ingredients:
- Chicken or Turkey (already cooked)
- Celery
- Onion
- Chicken or Turkey broth (homemade)
- Soy Sauce
- Corn Starch
- Black Pepper
- White Pepper
- Ground Ginger
- Kitchen Bouquet (for color)
- Optionally: Water chestnuts or other such veggies

directions:
- Take one part meat, one part celery and ~ 1/2 part or les onion, throw it in a pot, thorw in the broth and enough water to cover.
- Heat (do not boil) of low to low-medium heat for 45-60 minutes, covered. Stir occasionally
- Reduce heat to low and remove lid
- UIsing a container you can close and shake vigorously, combine equal parts corn starch and soy sauce. Overall quantity required is dependant on the size of the batch.
- Shake until well belnded. Pour into pot while stirring. Repeast as much as necessary until desired thickness is achieved.
- Splash in small amounts of Kitchen Bouquet until desired color is achieved
- Serve with rice and/or chow mein noodles

---

Recipe from Dennis O'Leary, passed down from his mother Helen.

