---

layout: recipe
title:  "Ham and Cheese Sliders Recipe"
image: hamandcheese.jpg
tags: mains, sandwich

ingredients:
- 24 slices of deli honey ham
- 6 Slices of swiss cheese, cut into fourths
- 1/3 cup mayonnaise
- 1 tablespoon poppy seeds
- 1 1/2 tablespoons** dijon mustard
- 1/2 cup butter melted
- 1 tablespoon Onion Powder
- 1/2 teaspoon worcestershire sauce
- 2 packages (12 count) KING'S HAWAIIAN Original Hawaiian Sweet Dinner Rolls

directions:
- Cut rolls in half and spread mayo onto 1 side of the rolls. Place a slice or two of ham and slice of swiss cheese in roll. Replace the top of the rolls and bunch them closely together into a baking dish.
- In a medium bowl, whisk together poppy seeds, dijon mustard, melted butter, onion powder and worcestershire sauce.
- Pour sauce over the rolls, just covering the tops. Cover with foil and let sit for 10 minutes.
- Bake at 350 degrees for 10 minutes or until cheese is melted. Uncover and cook for additional 2 minutes until tops are slightly browned and crisp. Serve warm.

---

Jenn's receipe