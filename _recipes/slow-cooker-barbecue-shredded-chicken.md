---

layout: recipe
title:  "Slow Cooker Barbecue Shredded Chicken"
image: bbq-chicken.jpg
tags: main, chicken

ingredients:
- 4 (4oz) boneless, skinless chicken breasts
- 1 cup catsup (free of high-fructose corn syrup)
- ½ cup of water
- 1 tablespoon freshly squeezed lemon juice
- 1 tablespoon dried onions
- ½ teaspoon dried mustard
- ¼ teaspoon red pepper flakes
- 3 tablespoons Worcestershire sauce
- 1 tablespoon white vinegar

directions:
- Place the chicken breasts in a slow cooker
- In a small bowl, whisk together the catsup, water, lemon juice, dried onions, dried mustard, red pepper, flakes, Worcestershire sauce, and white vinegar. Pour the mixture over the chicken
- Cover the slow cooker and turn on low to cook for 6 to 8 hours
- Transfer the chicken to a plate and shred it with a fork.
- Return chicken to slow cooker and cook on low for another 30 minutes

---

Per Serving (4ox): Calories: 188

Protein: 22g; Fat: 3g; Carbs 16g; Sugar 10g; Sodium: 750mg