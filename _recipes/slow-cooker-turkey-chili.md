---

layout: recipe
title:  "Slow Cooker Turkey Chili"
image: turkey-chili.jpg
tags: main, turkey

ingredients:
- 2 pounds extra-lean ground turkey
- 2 cans kidney beans, drained and rinsed
- 1 can diced tomatoes with green chiles
- 1 large onion, finely chopped
- 1 green bell pepper, finely chopped
- 2 celery stalks, finely chopped
- 4 teaspoons minced garlic
- 1 teaspoon dried oregano
- 2 tablespoons ground cumin
- 3 tablespoons chili powder
- 1 (8oz) can of tomato sauce

directions:
- Brown the ground turkey (7 to 9 minutes over medium-high heat)
- While the turkey browns, place the beans, tomatoes puree, onion, bell pepper, celery, garlic, oregano, cumin, chili powder, and tomatoe juice in the slow cooker.
- Stir in the ground turkey and mix well
- Cover the slow cooker and turn on low to cook for 8 hours
- Serve garnished with Greek yogurt, shredded cheddar cheese, and chopped scallions (if using)

---

Per Serving (1/2 cup): Calories: 140

Protein: 14g; Fat: 4g; Carbs 12g; Sugar 4g; Sodium: 280mg